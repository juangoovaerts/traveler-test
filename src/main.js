import Vue from 'vue'
import App from './App.vue'
import axios from 'axios';
import Dropdown from './components/common/Dropdown';


Vue.config.productionTip = false;
Vue.component("dropdown", Dropdown);

window.axios = axios;
window.Event = new Vue();

new Vue({
  render: h => h(App),
}).$mount('#app')
