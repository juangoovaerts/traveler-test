export class Cart {
    constructor(){
        this.products = [];
        Event.$on('productBuyed', (product) => this.addProduct(product));
    }

    addProduct(product){
        this.products.push(product);
    }
}