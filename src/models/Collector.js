import collect from 'collect.js';

export class Collector{

    constructor(
        categories,
        products,
    ){
        this.categorias = categories;
        this.products = products;
        this.collect = collect;
    }

}